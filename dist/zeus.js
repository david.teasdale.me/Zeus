"use strict";

exports.__esModule = true;
exports.Dictator = exports.Input = exports.Base = undefined;

var _createClass = function () { function defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } } return function (Constructor, protoProps, staticProps) { if (protoProps) defineProperties(Constructor.prototype, protoProps); if (staticProps) defineProperties(Constructor, staticProps); return Constructor; }; }();

var _fableCore = require("fable-core");

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

var Base = exports.Base = function ($exports) {
    var Value = $exports.Value = function () {
        function Value(caseName, fields) {
            _classCallCheck(this, Value);

            this.Case = caseName;
            this.Fields = fields;
        }

        Value.prototype.Equals = function Equals(other) {
            return _fableCore.Util.equalsUnions(this, other);
        };

        Value.prototype.CompareTo = function CompareTo(other) {
            return _fableCore.Util.compareUnions(this, other);
        };

        return Value;
    }();

    _fableCore.Util.setInterfaces(Value.prototype, ["FSharpUnion", "System.IEquatable", "System.IComparable"], "Zeus.Base.Value");

    var Operator = $exports.Operator = function () {
        function Operator(caseName, fields) {
            _classCallCheck(this, Operator);

            this.Case = caseName;
            this.Fields = fields;
        }

        Operator.prototype.Equals = function Equals(other) {
            return _fableCore.Util.equalsUnions(this, other);
        };

        Operator.prototype.CompareTo = function CompareTo(other) {
            return _fableCore.Util.compareUnions(this, other);
        };

        return Operator;
    }();

    _fableCore.Util.setInterfaces(Operator.prototype, ["FSharpUnion", "System.IEquatable", "System.IComparable"], "Zeus.Base.Operator");

    var createOperator = $exports.createOperator = function createOperator(op) {
        return op === "GreaterThan" ? function (y) {
            return function (x) {
                return x.CompareTo(y) >= 0;
            };
        } : op === "LessThan" ? function (y) {
            return function (x) {
                return x.CompareTo(y) < 0;
            };
        } : op === "EqualTo" ? function (y) {
            return function (x) {
                return x.Equals(y);
            };
        } : op === "ProvidedAnAnswer" ? function (y) {
            return function (x) {
                return y.Case === "String" ? y.Fields[0].length > 0 : function () {
                    throw "ProvidedAnAnswer only handles strings";
                }();
            };
        } : op === "Contains" ? function (y) {
            return function (x) {
                var matchValue = [y, x];

                var $target1 = function $target1() {
                    throw "x and y must be a String";
                };

                if (matchValue[0].Case === "String") {
                    if (matchValue[1].Case === "String") {
                        var x_1 = matchValue[1].Fields[0];
                        var y_1 = matchValue[0].Fields[0];
                        return x_1.indexOf(y_1) >= 0;
                    } else {
                        return $target1();
                    }
                } else {
                    return $target1();
                }
            };
        } : _fableCore.String.fsFormat("Unknown operator %O")(function (x) {
            throw x;
        })(op);
    };

    var Answer = $exports.Answer = function () {
        function Answer(value, questionId, pageId) {
            _classCallCheck(this, Answer);

            this.value = value;
            this.questionId = questionId;
            this.pageId = pageId;
        }

        Answer.prototype.Equals = function Equals(other) {
            return _fableCore.Util.equalsRecords(this, other);
        };

        Answer.prototype.CompareTo = function CompareTo(other) {
            return _fableCore.Util.compareRecords(this, other);
        };

        return Answer;
    }();

    _fableCore.Util.setInterfaces(Answer.prototype, ["FSharpRecord", "System.IEquatable", "System.IComparable"], "Zeus.Base.Answer");

    return $exports;
}({});

var Input = exports.Input = function ($exports) {
    var InputRuleType = $exports.InputRuleType = function () {
        function InputRuleType(code, value) {
            _classCallCheck(this, InputRuleType);

            this.code = code;
            this.value = value;
        }

        InputRuleType.prototype.Equals = function Equals(other) {
            return _fableCore.Util.equalsRecords(this, other);
        };

        InputRuleType.prototype.CompareTo = function CompareTo(other) {
            return _fableCore.Util.compareRecords(this, other);
        };

        return InputRuleType;
    }();

    _fableCore.Util.setInterfaces(InputRuleType.prototype, ["FSharpRecord", "System.IEquatable", "System.IComparable"], "Zeus.Input.InputRuleType");

    var InputValueType = $exports.InputValueType = function () {
        function InputValueType(code, value) {
            _classCallCheck(this, InputValueType);

            this.code = code;
            this.value = value;
        }

        InputValueType.prototype.Equals = function Equals(other) {
            return _fableCore.Util.equalsRecords(this, other);
        };

        InputValueType.prototype.CompareTo = function CompareTo(other) {
            return _fableCore.Util.compareRecords(this, other);
        };

        return InputValueType;
    }();

    _fableCore.Util.setInterfaces(InputValueType.prototype, ["FSharpRecord", "System.IEquatable", "System.IComparable"], "Zeus.Input.InputValueType");

    var InputRule = $exports.InputRule = function () {
        function InputRule(answerValue, questionId, ruleType, pageId) {
            _classCallCheck(this, InputRule);

            this.answerValue = answerValue;
            this.questionId = questionId;
            this.ruleType = ruleType;
            this.pageId = pageId;
        }

        InputRule.prototype.Equals = function Equals(other) {
            return _fableCore.Util.equalsRecords(this, other);
        };

        InputRule.prototype.CompareTo = function CompareTo(other) {
            return _fableCore.Util.compareRecords(this, other);
        };

        return InputRule;
    }();

    _fableCore.Util.setInterfaces(InputRule.prototype, ["FSharpRecord", "System.IEquatable", "System.IComparable"], "Zeus.Input.InputRule");

    var InputGroup = $exports.InputGroup = function () {
        function InputGroup(id, rules) {
            _classCallCheck(this, InputGroup);

            this.id = id;
            this.rules = rules;
        }

        InputGroup.prototype.Equals = function Equals(other) {
            return _fableCore.Util.equalsRecords(this, other);
        };

        InputGroup.prototype.CompareTo = function CompareTo(other) {
            return _fableCore.Util.compareRecords(this, other);
        };

        return InputGroup;
    }();

    _fableCore.Util.setInterfaces(InputGroup.prototype, ["FSharpRecord", "System.IEquatable", "System.IComparable"], "Zeus.Input.InputGroup");

    var InputPage = $exports.InputPage = function () {
        function InputPage(id, constraints) {
            _classCallCheck(this, InputPage);

            this.id = id;
            this.constraints = constraints;
        }

        InputPage.prototype.Equals = function Equals(other) {
            return _fableCore.Util.equalsRecords(this, other);
        };

        InputPage.prototype.CompareTo = function CompareTo(other) {
            return _fableCore.Util.compareRecords(this, other);
        };

        return InputPage;
    }();

    _fableCore.Util.setInterfaces(InputPage.prototype, ["FSharpRecord", "System.IEquatable", "System.IComparable"], "Zeus.Input.InputPage");

    var InputQuestionnaire = $exports.InputQuestionnaire = function () {
        function InputQuestionnaire(id, pages) {
            _classCallCheck(this, InputQuestionnaire);

            this.id = id;
            this.pages = pages;
        }

        InputQuestionnaire.prototype.Equals = function Equals(other) {
            return _fableCore.Util.equalsRecords(this, other);
        };

        InputQuestionnaire.prototype.CompareTo = function CompareTo(other) {
            return _fableCore.Util.compareRecords(this, other);
        };

        return InputQuestionnaire;
    }();

    _fableCore.Util.setInterfaces(InputQuestionnaire.prototype, ["FSharpRecord", "System.IEquatable", "System.IComparable"], "Zeus.Input.InputQuestionnaire");

    return $exports;
}({});

var Dictator = exports.Dictator = function ($exports) {
    var Rule = $exports.Rule = function () {
        function Rule(op, questionId, pageId) {
            _classCallCheck(this, Rule);

            this.op = op;
            this.questionId = questionId;
            this.pageId = pageId;
        }

        _createClass(Rule, [{
            key: "Operator",
            get: function get() {
                return this.op;
            }
        }, {
            key: "QuestionId",
            get: function get() {
                return this.questionId;
            }
        }, {
            key: "PageId",
            get: function get() {
                return this.pageId;
            }
        }]);

        return Rule;
    }();

    _fableCore.Util.setInterfaces(Rule.prototype, [], "Zeus.Dictator.Rule");

    var Group = $exports.Group = function () {
        function Group(rules) {
            _classCallCheck(this, Group);

            this.rules = rules;
        }

        Group.prototype.IsTrue = function IsTrue(answers) {
            return this.Rules.every(function (x) {
                var retV = _fableCore.Seq.tryFind(function (i) {
                    return i.questionId === x.QuestionId ? i.pageId === x.PageId : false;
                }, answers);

                if (_fableCore.Util.equals(retV)) {
                    return false;
                } else {
                    return x.Operator(retV.value);
                }
            });
        };

        Group.prototype.createRule = function createRule(inputRule) {
            var op = Base.createOperator(inputRule.ruleType.value)(inputRule.answerValue.value);
            return new Rule(op, inputRule.questionId, inputRule.pageId);
        };

        Group.prototype.createRules = function createRules(inputRules) {
            var _this = this;

            return inputRules.map(function (inputRule) {
                return _this.createRule(inputRule);
            });
        };

        _createClass(Group, [{
            key: "Rules",
            get: function get() {
                return this.createRules(this.rules);
            }
        }]);

        return Group;
    }();

    _fableCore.Util.setInterfaces(Group.prototype, [], "Zeus.Dictator.Group");

    var Page = $exports.Page = function () {
        function Page(id, groups) {
            _classCallCheck(this, Page);

            this.id = id;
            this.groups = groups;
        }

        Page.prototype.createGroupsArray = function createGroupsArray(groups) {
            var arr = Array.from(_fableCore.Seq.sortWith(function (x, y) {
                return _fableCore.Util.compare(function (x) {
                    return x.id;
                }(x), function (x) {
                    return x.id;
                }(y));
            }, groups));
            return arr.map(function (x) {
                return new Group(x.rules);
            });
        };

        _createClass(Page, [{
            key: "PageId",
            get: function get() {
                return this.id;
            }
        }, {
            key: "Groups",
            get: function get() {
                return this.createGroupsArray(this.groups);
            }
        }]);

        return Page;
    }();

    _fableCore.Util.setInterfaces(Page.prototype, [], "Zeus.Dictator.Page");

    var Manifest = $exports.Manifest = function () {
        function Manifest(questionnairePageRules) {
            _classCallCheck(this, Manifest);

            this.questionnairePageRules = questionnairePageRules;
        }

        Manifest.prototype.CanView = function CanView(pageId, answers) {
            var page = this.Pages.find(function (x) {
                return x.PageId === pageId;
            });

            if (page.Groups.length === 0) {
                return true;
            } else {
                return _fableCore.Seq.fold(function (acc, item) {
                    return item.IsTrue(answers) ? true : acc;
                }, false, page.Groups);
            }
        };

        Manifest.prototype.createPagesArray = function createPagesArray(pageRules) {
            var arr = Array.from(_fableCore.Seq.sortWith(function (x, y) {
                return _fableCore.Util.compare(function (x) {
                    return x.id;
                }(x), function (x) {
                    return x.id;
                }(y));
            }, pageRules));
            return arr.map(function (x) {
                return new Page(x.id, x.constraints);
            });
        };

        _createClass(Manifest, [{
            key: "Pages",
            get: function get() {
                var pageRules = this.questionnairePageRules.pages;
                return this.createPagesArray(pageRules);
            }
        }]);

        return Manifest;
    }();

    _fableCore.Util.setInterfaces(Manifest.prototype, [], "Zeus.Dictator.Manifest");

    var createManifest = $exports.createManifest = function createManifest(questionnairePageRules) {
        return new Manifest(questionnairePageRules);
    };

    var printOperator = $exports.printOperator = function printOperator(input) {
        return input === "GreaterThan" ? "GreaterThen" : input === "LessThan" ? "LessThen" : input === "EqualTo" ? "EqualTo" : input === "Contains" ? "Contains" : function () {
            throw "Unknown operator";
        }();
    };

    return $exports;
}({});
//# sourceMappingURL=zeus.js.map