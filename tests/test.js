var assert = require('chai').assert;
var expect = require('chai').expect;
var lib = require('../dist/zeus');

var questionnaire = {
    "id": "9000000000000000041",
    "clinicalQuestId": "7",
    "name": null,
    "pages": [
        {
            "id": 1,
            "questions": [
                {
                    "id": "15",
                    "description": "Physical Activity in the Past Week",
                    "clinicalDataId": "41",
                    "type": "CodedSet",
                    "measurement": {
                        "code": "307",
                        "value": "Physical Activity in the Past Week",
                        "unitType": []
                    },
                    "options": [
                        {
                            "code": "21",
                            "value": "Very heavy, (for example) run, at fast pace"
                        },
                        {
                            "code": "22",
                            "value": "Heavy, (for example) jog, at a slow pace"
                        },
                        {
                            "code": "23",
                            "value": "Moderate, (for example) walk, at a fast pace"
                        },
                        {
                            "code": "24",
                            "value": "Light, (for example) walk, at a medium pace"
                        },
                        {
                            "code": "25",
                            "value": "Very light, (for example) walk, at a slow pace or not able to walk"
                        }
                    ]
                }
            ],
            "constraints": []
        },
        {
            "id": 2,
            "questions": [
                {
                    "id": "16",
                    "description": "Emotional Distress in the Past Week",
                    "clinicalDataId": "42",
                    "type": "CodedSet",
                    "measurement": {
                        "code": "315",
                        "value": "Emotional Distress in the Past Week",
                        "unitType": []
                    },
                    "options": [
                        {
                            "code": "26",
                            "value": "Not at all"
                        },
                        {
                            "code": "27",
                            "value": "Slightly"
                        },
                        {
                            "code": "28",
                            "value": "Moderately"
                        },
                        {
                            "code": "29",
                            "value": "Quite a bit"
                        },
                        {
                            "code": "30",
                            "value": "Extremely"
                        }
                    ]
                }
            ],
            "constraints": [
                {
                    "id": "1",
                    "rules": [
                        {
                            "id": "77",
                            "questionId": "15",
                            "ruleType": {
                                "id": "240",
                                "value": "EqualTo"
                            },
                            "answerValue": {
                                "type": "CodedSet",
                                "value": "23"
                            },
                            "pageId": 1
                        }
                    ]
                },
                {
                    "id": "2",
                    "rules": [
                        {
                            "id": "78",
                            "questionId": "15",
                            "ruleType": {
                                "id": "240",
                                "value": "EqualTo"
                            },
                            "answerValue": {
                                "type": "CodedSet",
                                "value": "24"
                            },
                            "pageId": 1
                        }
                    ]
                }
            ]
        },
        {
            "id": 3,
            "questions": [
                {
                    "id": "17",
                    "description": "Difficulty Doing Daily Tasks",
                    "clinicalDataId": "43",
                    "type": "CodedSet",
                    "measurement": {
                        "code": "323",
                        "value": "Difficulty Doing Daily Tasks",
                        "unitType": []
                    },
                    "options": [
                        {
                            "code": "31",
                            "value": "No Difficulty at all"
                        },
                        {
                            "code": "32",
                            "value": "A little bit of difficulty"
                        },
                        {
                            "code": "33",
                            "value": "Some difficulty"
                        },
                        {
                            "code": "34",
                            "value": "Much difficulty"
                        },
                        {
                            "code": "35",
                            "value": "Could not do"
                        }
                    ]
                }
            ],
            "constraints": []
        },
        {
            "id": 4,
            "questions": [
                {
                    "id": "18",
                    "description": "Limitation of Social Activities",
                    "clinicalDataId": "44",
                    "type": "CodedSet",
                    "measurement": {
                        "code": "325",
                        "value": "Limitation of Social Activities",
                        "unitType": []
                    },
                    "options": [
                        {
                            "code": "26",
                            "value": "Not at all"
                        },
                        {
                            "code": "27",
                            "value": "Slightly"
                        },
                        {
                            "code": "28",
                            "value": "Moderately"
                        },
                        {
                            "code": "29",
                            "value": "Quite a bit"
                        },
                        {
                            "code": "30",
                            "value": "Extremely"
                        }
                    ]
                }
            ],
            "constraints": []
        },
        {
            "id": 5,
            "questions": [
                {
                    "id": "19",
                    "description": "Health Improvement Over 1 Week",
                    "clinicalDataId": "45",
                    "type": "CodedSet",
                    "measurement": {
                        "code": "333",
                        "value": "Health Improvement Over 1 Week",
                        "unitType": []
                    },
                    "options": [
                        {
                            "code": "36",
                            "value": "Much Better"
                        },
                        {
                            "code": "37",
                            "value": "A little better"
                        },
                        {
                            "code": "38",
                            "value": "About the same"
                        },
                        {
                            "code": "39",
                            "value": "A little worse"
                        },
                        {
                            "code": "40",
                            "value": "Much worse"
                        }
                    ]
                }
            ],
            "constraints": []
        },
        {
            "id": 6,
            "questions": [
                {
                    "id": "20",
                    "description": "Health Rating in the Past Week",
                    "clinicalDataId": "46",
                    "type": "CodedSet",
                    "measurement": {
                        "code": "341",
                        "value": "Health Rating in the Past Week",
                        "unitType": []
                    },
                    "options": [
                        {
                            "code": "41",
                            "value": "Excellent"
                        },
                        {
                            "code": "42",
                            "value": "Very good"
                        },
                        {
                            "code": "43",
                            "value": "Good"
                        },
                        {
                            "code": "44",
                            "value": "Fair"
                        },
                        {
                            "code": "45",
                            "value": "Poor"
                        }
                    ]
                }
            ],
            "constraints": [
                {
                    "id": "1",
                    "rules": [
                        {
                            "id": "79",
                            "questionId": "15",
                            "ruleType": {
                                "id": "240",
                                "value": "EqualTo"
                            },
                            "answerValue": {
                                "type": "CodedSet",
                                "value": "21"
                            },
                            "pageId": 1
                        },
                        {
                            "id": "80",
                            "questionId": "15",
                            "ruleType": {
                                "id": "240",
                                "value": "EqualTo"
                            },
                            "answerValue": {
                                "type": "CodedSet",
                                "value": "21"
                            },
                            "pageId": 1
                        },
                        {
                            "id": "81",
                            "questionId": "15",
                            "ruleType": {
                                "id": "240",
                                "value": "EqualTo"
                            },
                            "answerValue": {
                                "type": "CodedSet",
                                "value": "21"
                            },
                            "pageId": 1
                        }
                    ]
                }
            ]
        }
    ]
}

describe('ParseOperator', function () {
    describe('#printOperator()', function () {
        it('should return EqualTo when operator is equalTo', function () {
            assert.equal("EqualTo", lib.Dictator.printOperator("EqualTo"))
        });
        it('should return GreaterThen when operator is greaterThen', function () {
            assert.equal("GreaterThen", lib.Dictator.printOperator("GreaterThen"))
        });
        it('should return LessThen when operator is lessThen', function () {
            assert.equal("LessThen", lib.Dictator.printOperator("LessThen"))
        });
        it('should return Contains when operator is contains', function () {
            assert.equal("Contains", lib.Dictator.printOperator("Contains"))
        });
        it('should throw an error', function () {
            expect(function () { lib.Dictator.printOperator("bob") }).to.throw("Unknown operator")
        });
    });

    describe('#Manifest.CanView()', function () {
        var manifest = lib.Dictator.createManifest(questionnaire);
        it('should return true when correct first group is all right', function () {
            assert.equal(true, manifest.CanView(2, [
                {
                    "questionId": "15",
                    "value": "23",
                    "pageId": 1
                }
            ]))
        });
        it('should return true when correct second group is all right', function () {
            assert.equal(true, manifest.CanView(2, [{
                "questionId": "15",
                "value": "24",
                "pageId": 1
            },
            ]))
        });
        it('should return false when correct both groups are wrong', function () {
            assert.equal(false, manifest.CanView(2, [
                {
                    "pageId": 1,
                    "questionId": "15",
                    "value": "3"
                },
                {
                    "pageId": 1,
                    questionId: "15",
                    "value": "5"
                }
            ]))
        });
        it('should return true when there are no rules', function () {
            assert.equal(true, manifest.CanView(1, [
                {
                    PageId: 1,
                    QuestionId: "15",
                    Value: 5
                }]))
        });
        it('should throw an error', function () {
            expect(function () {
                manifest.CanView(6, [
                    {
                        QuestionId: "15",
                        PageId: 1,
                        Value: 9
                    }
                ])
            }).to.throw("x and y must be a String")
        });
    })
});